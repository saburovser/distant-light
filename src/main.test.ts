import { Main } from './main';

test('creates Main instance', () => {
  expect(new Main()).toBeInstanceOf(Main);
});

test('gets message from Main instance', () => {
  const main = new Main();
  const message = main.getMessage();
  expect(message).toBe('Message');
});
